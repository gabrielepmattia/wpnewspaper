<?php
// =============================================
//  functions.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 14, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

function wpnewspaper_setup() {
    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
    add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption',));
    add_theme_support('post-thumbnails');
    //add_theme_support('post-formats', array('aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat',));
}

add_action('after_setup_theme', 'wpnewspaper_setup');

// Remove totally useless emoji support
function disable_wp_emojicons() {
    // all actions related to emojis
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    // filter to remove TinyMCE emojis
    add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');
}

function disable_emojicons_tinymce($plugins) {
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

add_action('init', 'disable_wp_emojicons');

function enqueue_theme_files() {
  $my_theme = wp_get_theme();
  wp_enqueue_style( 'style', get_stylesheet_uri(), false, $my_theme->get( 'Version' ), 'all');
  wp_enqueue_style( 'fonts', get_template_directory_uri() . "/fonts/fonts.css", false, $my_theme->get( 'Version' ), 'all');
  wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/libs/css/font-awesome.min.css', false, '4.7.0', 'all');
  //wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_files' );

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
  global $post;
	return ' [...] <a href="'. get_permalink($post->ID) . '">'. __("Continua a leggere &raquo;", "wpnewspaper") .'</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// ===== Sidebars
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Home Sidebar Sinistra', 'wpnewspaper' ),
        'id' => 'home-left-sidebar',
        'description' => __( 'Sidebar sinistra della Home Page', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
      	'after_widget'  => '</li>',
      	'before_title'  => '<h2 class="widgettitle">',
      	'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Home Sidebar Destra', 'wpnewspaper' ),
        'id' => 'home-right-sidebar',
        'description' => __( 'Sidebar destra della Home Page', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
      	'after_widget'  => '</li>',
      	'before_title'  => '<h2 class="widgettitle">',
      	'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init' );

// ===== Customizer, widgets, ecc
require_once(get_template_directory() . "/customizer/index.php");
require_once(get_template_directory() . "/widgets/index.php");
require_once(get_template_directory() . "/menus/index.php");
