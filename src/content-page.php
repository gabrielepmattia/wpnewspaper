<?php
// =============================================
//  content-page.php
// =============================================

/**
 * @Project:      ffast
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Mar 07, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Mar 07, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */
?>

 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 	<?php
 		// Post thumbnail.
 		//twentyfifteen_post_thumbnail();
 	?>

 	<header class="entry-header">
 		<?php
 			if ( is_single() ) {
 				the_title( '<h1 class="main-title">', '</h1>' );
 				if(function_exists('the_subtitle')) the_subtitle( '<h2 class="main-subtitle">', '</h2>' );
 			} else
 				the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' );
 		?>

     <?php if( has_post_thumbnail() ) { ?>
     <div class="post-thumbnail-container" style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>')"></div>
     <?php } ?>
 	</header><!-- .entry-header -->

 	<div class="entry-content">
 		<?php
 			the_content( sprintf(
         	__( 'Continua a leggere %s', 'wpnewspaper' ),
 				the_title( '<span class="screen-reader-text">', '</span>', false )
 			) );

    ?>
 	</div><!-- .entry-content -->

 	<?php
 		// Author bio.
 		if ( is_single() && get_the_author_meta( 'description' ) ) :
 			get_template_part( 'author-bio' );
 		endif;
 	?>

 	<footer class="entry-footer">
 		<?php edit_post_link( __( 'Modifica', 'wpnewspaper' ), '<span class="edit-link">', '</span>' ); ?>
 	</footer><!-- .entry-footer -->

 </article><!-- #post-## -->
