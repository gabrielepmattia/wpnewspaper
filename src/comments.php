<?php
// =============================================
//  comments.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 09, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

//Get only the approved comments
$args = array(
    'status' => 'approve'
);

// The comment Query
$comments_query = new WP_Comment_Query;
$comments = $comments_query->query( $args );

// Comment Loop
if ( $comments ) {
    foreach ( $comments as $comment ) {
        echo '<p>' . $comment->comment_content . '</p>';
    }
} else {
    echo 'Nessun commento a questo articolo.';
}
?>
