<?php
// =============================================
//  index.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 19, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

?>

<?php get_header(); ?>

<section class="index">
	<div class="col col15 coll floatl"><?php get_sidebar('left'); ?></div> <!-- left col -->

	<div class="col col60 colc floatl">

		<?php if (have_posts()) : ?>

			<?php if (is_home() && !is_front_page()) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<?php while (have_posts()) : the_post();
				get_template_part('content', 'short');
			endwhile; ?>

		<?php
			// Previous/next page navigation.
			the_posts_pagination(array(
				'prev_text'          => '&laquo;',
				'next_text'          => '&raquo;',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Pagina', 'wpnewspaper') . ' </span>',
			));

		// If no content, include the "No posts found" template.
		else :
			get_template_part('content', 'none');

		endif;
		?>


	</div> <!-- center col -->

	<div class="col col25 colr floatl"><?php get_sidebar('right'); ?>
</section> <!-- right col -->
</div>

<?php get_footer(); ?>