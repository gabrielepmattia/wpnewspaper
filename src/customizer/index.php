<?php
// =============================================
//  customizer.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 19, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

function wpnewspaper_customize_register($wp_customize)
{
    // All our sections, settings, and controls will be added here

    // Sections
    $wp_customize->add_section(
        'wpnewspaper_copyright_section', array(
        'title'      => __('Copyright', 'wpnewspaper'),
        'priority'   => 1000,
        )
    );

    // Settings
    $wp_customize->add_setting(
        'footer_copyright_text', array(
        'default'     => __('WPNewspaper Theme by <a href="'. "https://gpm.name" .'">@gabrielepmattia</a>', 'wpnewspaper'),
        'transport'   => 'refresh',
        )
    );

    $wp_customize->add_setting(
        'footer_disclaimer_text', array(
        'default'     => __('Rilasciato sotto licenza <i>GNU General Public License v3</i>. <a href="'. "https://gitlab.com/gabrielepmattia/wpnewspaper" .'">Sorgente'. " &raquo;" .'</a>', 'wpnewspaper'),
        'transport'   => 'refresh',
        )
    );

    // Controls
    $wp_customize->add_control(
        'footer_copyright_text_control',
        array(
        'label'    => __('Testo copyright pié di pagina', 'wpnewspaper'),
        'section'  => 'wpnewspaper_copyright_section',
        'settings' => 'footer_copyright_text',
        'type'     => 'text'
        )
    );

    $wp_customize->add_control(
        'footer_disclaimer_text_control',
        array(
        'label'    => __('Testo disclaimer pié di pagina', 'wpnewspaper'),
        'section'  => 'wpnewspaper_copyright_section',
        'settings' => 'footer_disclaimer_text',
        'type'     => 'text'
        )
    );

}

 add_action('customize_register', 'wpnewspaper_customize_register');
