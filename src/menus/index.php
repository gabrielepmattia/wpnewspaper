<?php
// =============================================
//  index.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 10, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 10, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

 // Register all menus
 register_nav_menus( array(
 	'main_navigation' => esc_html__( 'Menu di navigazione principale, sotto la testata', 'wpnewspaper' ),
  'top_right_menu' => esc_html__( 'Menu in alto a destra, di default "Accedi o Registrati"', 'wpnewspaper' )
 ) );

require_once('top-right.php'); // Top right menu
