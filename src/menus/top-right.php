<?php
// =============================================
//  top-right.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 10, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 13, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

 function top_right_handler(){
   global $wp;
   $current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );

   if(is_user_logged_in()) {
     $current_user = wp_get_current_user();
     echo __('Ciao', 'wpnewspaper') . ',' .
       ' <i>'. $current_user->user_login .'</i>.' .
       ' <a href="' . wp_logout_url($current_url) . '">' . __('Logout', 'wpnewspaper') . '&raquo;</a>'
       ;
   } else echo '<a href="'. wp_login_url($current_url) .'">' . __('Accedi', 'wpnewspaper') . "</a>" .
               ' o ' .
               '<a href="'. wp_registration_url() .'">' . __('Registrati', 'wpnewspaper') . "</a>";
 }
