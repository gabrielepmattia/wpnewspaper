<?php
// =============================================
//  footer.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 19, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

 ?>

</div> <!-- content -->

<footer class="mastfooter">
  <?php wp_footer(); ?>
  <div class="sized-width-content">
    <p class="copyright"><?php echo get_theme_mod( 'footer_copyright_text' ) ?></p>
    <p class="disclaimer"><?php echo get_theme_mod( 'footer_disclaimer_text' ) ?></p>
  </div>
</footer>
</body>
</html>
