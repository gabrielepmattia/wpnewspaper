<?php
// =============================================
//  page.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Mar 07, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Mar 07, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */
?>

 <?php get_header(); ?>
   <div class="col">
     <?php
   		// Start the loop.
   		while ( have_posts() ) : the_post();

   			/*
   			 * Include the post format-specific template for the content. If you want to
   			 * use this in a child theme, then include a file called called content-___.php
   			 * (where ___ is the post format) and that will be used instead.
   			 */
   			get_template_part( 'content', get_post_format() );

   			// If comments are open or we have at least one comment, load up the comment template.
   			if ( comments_open() || get_comments_number() ) :
   				//comments_template();
   			endif;

   			// Previous/next post navigation.
   			/*the_post_navigation( array(
   				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'wpnewspaper' ) . '</span> ' .
   					'<span class="screen-reader-text">' . __( 'Next post:', 'wpnewspaper' ) . '</span> ' .
   					'<span class="post-title">%title</span>',
   				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'wpnewspaper' ) . '</span> ' .
   					'<span class="screen-reader-text">' . __( 'Previous post:', 'wpnewspaper' ) . '</span> ' .
   					'<span class="post-title">%title</span>',
   			) );
 */
   		// End the loop.
   		endwhile;
   		?>

   </div>

 <?php get_footer(); ?>
