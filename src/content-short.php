<?php
// =============================================
//  content-short.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 13, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php // twentyfifteen_post_thumbnail(); ?>

<header class="entry-header-short">
	<?php
		// Title & subtitle
		the_title( sprintf( '<h1><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		if(function_exists('the_subtitle')) the_subtitle( '<h2>', '</h2>' );

		if (get_post_type() == 'post')
			edit_post_link( __( 'Modifica', 'wpnewspaper' ), '<span class="edit-link">', '</span>' );
		else
			edit_post_link( __( 'Modifica', 'wpnewspaper' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' );
	?>
</header>

	<?php if( has_post_thumbnail() ) { ?>
	<div class="post-thumbnail-container" style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>')"></div>
	<?php } ?>

	<?php the_excerpt(); ?>

</article><!-- #post-## -->
