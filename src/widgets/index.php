<?php
// =============================================
//  index.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 10, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

require_once('posts_from_catecory.php'); // posts from category widget
