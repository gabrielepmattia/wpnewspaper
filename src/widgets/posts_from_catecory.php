<?php
// =============================================
//  posts_from_catecory.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 10, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

 class PostsFromCategory extends WP_Widget {

 /**
	 * Sets up the widgets name etc
  */
  function __construct() {
    $widget_ops = array(
   	  'classname' => 'posts_from_catecory',
   		'description' => __('Mostra gli articoli da una categoria specifica', 'wpnewspaper'),
   	);

    parent::__construct( 'posts_from_catecory', esc_html__( 'Articoli da categoria', 'wpnewspaper' ), $widget_ops );
  }

 	/**
 	 * Outputs the content of the widget
 	 *
 	 * @param array $args
 	 * @param array $instance
 	 */
 	public function widget( $args, $instance ) {
      echo '<h2 class="widgettitle">' . $instance['title'] . '</h2>';

      $qargs = array(
        'cat' => $instance['cats'],
        'posts_per_page' => $instance['max']
      );
      $query = new WP_Query( $qargs );
      while( $query->have_posts()) : $query->the_post(); get_template_part( 'content', 'short' ); endwhile;
 	}

 	/**
 	 * Outputs the options form on admin
 	 *
 	 * @param array $instance The widget options
 	 */
 	public function form( $instance ) {
    $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Titolo widget', 'wpnewspaper' );
    $cats = ! empty( $instance['cats'] ) ? $instance['cats'] : "0";
    $max = ! empty( $instance['max'] ) ? $instance['max'] : "5";
  ?>
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Titolo:', 'wpnewspaper' ); ?></label>
      <input class="widefat"
        id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
        name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
        type="text"
        value="<?php echo esc_attr( $title ); ?>">

      <label for="<?php echo esc_attr( $this->get_field_id( 'cats' ) ); ?>"><?php esc_attr_e( 'Categorie (es. 1,3,4):', 'wpnewspaper' ); ?></label>
      <input class="widefat"
        id="<?php echo esc_attr( $this->get_field_id( 'cats' ) ); ?>"
        name="<?php echo esc_attr( $this->get_field_name( 'cats' ) ); ?>"
        type="text"
        value="<?php echo esc_attr( $cats ); ?>">

      <label for="<?php echo esc_attr( $this->get_field_id( 'max' ) ); ?>"><?php esc_attr_e( 'Numero articoli da mostrare:', 'wpnewspaper' ); ?></label>
      <input class="widefat"
        id="<?php echo esc_attr( $this->get_field_id( 'max' ) ); ?>"
        name="<?php echo esc_attr( $this->get_field_name( 'max' ) ); ?>"
        type="number"
        value="<?php echo esc_attr( $max ); ?>">
    </p>
  <?php
 	}

 	/**
 	 * Processing widget options on save
 	 *
 	 * @param array $new_instance The new options
 	 * @param array $old_instance The previous options
 	 */
 	public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['cats'] = ( ! empty( $new_instance['cats'] ) ) ? strip_tags( $new_instance['cats'] ) : '0';
    $instance['max'] = ( ! empty( $new_instance['max'] ) ) ? strip_tags( $new_instance['max'] ) : '5';

    return $instance;
 	}
 }

 add_action( 'widgets_init', function(){
	register_widget( 'PostsFromCategory' );
});
