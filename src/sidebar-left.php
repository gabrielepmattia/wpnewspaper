<?php
// =============================================
//  sidebar-left.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 09, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

?>

 <div class="sidebar sidebar-left">

 	<?php if ( is_active_sidebar( 'home-left-sidebar' ) ) : ?>
 		<div id="widget-area" class="widget-area" role="complementary">
 			<?php dynamic_sidebar( 'home-left-sidebar' ); ?>
 		</div><!-- .widget-area -->
  <?php else :?>
    <?php
      $args = array('posts_per_page' => 0, 'paged' => $paged);
      $query = new WP_Query( $args );
      while( $query->have_posts()) : $query->the_post(); get_template_part( 'content', 'short' ); endwhile;
    ?>
 	<?php endif; ?>

 </div><!-- .sidebar -->
