<?php
// =============================================
//  header.php
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 10, 2017
 * @License:      GNU Public License 3.0
 * @Copyright:    See /LICENSE for full license text
 */

 ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>

    <?php wp_head(); ?>

    <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital,wght@0,400;0,700;1,400&family=Prata&family=Roboto+Condensed:wght@400;700&display=swap" rel="stylesheet"> 
</head>

<body>

<header class="masthead">
  <div class="top">
    <div class="sized-width-content">
      <div class="floatl coll"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date_i18n(get_option( 'date_format' ));  ?></div>
      <div class="floatr colr">

        <ul class="inline">
          <?php
          wp_nav_menu( array(
            'menu' => 'top_right_menu',
            'fallback_cb' => 'top_right_handler' // See ./menus/top-right.php
          ) ) ?>
        </ul>

      </div>
    </div>
  </div>
  <div class="center">
    <div class="sized-width-content">
      <!-- <div class="coll floatl col33">&nbsp;</div> -->
      <div class="colc floatl col100">
        <h1><a href="<?php echo  get_home_url(); ?>"><?php echo bloginfo('name'); ?></a></h1>
        <h2><?php echo bloginfo('description'); ?></h2>
      </div>
      <!-- <div class="colr floatl col33">&nbsp;</div> -->
    </div>
  </div>
  <div class="bottom">
    <nav class="bottom-top">
      <ul>

      <?php wp_nav_menu( array(
        'menu' => 'pages_navigation',
        'fallback_cb' => wp_list_pages(array(
          'title_li'    => "",
        ))
      ) ) ?>

      </ul>
    </nav>
    <nav class="bottom-bottom">
      <ul>

      <?php wp_nav_menu( array(
        'menu' => 'main_navigation',
        'fallback_cb' => wp_list_categories(array(
          'hide_empty'  => 1,
          'title_li'    => "",
        ))
      ) ) ?>

      </ul>
    </nav>
  </div>

  <?php if(is_home()) : // Display marquee if home ?>
  <div class="sized-width-content breaking-news">
    <div class="marquee">
      <p>
      <?php
        $args = array(
        	'numberposts' => 10,
        	'offset' => 0,
        	'category' => 0,
        	'orderby' => 'post_date',
        	'order' => 'DESC',
        	'include' => '',
        	'exclude' => '',
        	'meta_key' => '',
        	'meta_value' =>'',
        	'post_type' => 'post',
        	'post_status' => 'publish',
        	'suppress_filters' => true
        );

        $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

        foreach( $recent_posts as $recent ){
    		  echo '<a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> &nbsp;&nbsp;&nbsp;';
    	  }
    	  wp_reset_query(); ?>

      </p>
    </div>

  </div>
<?php endif ?>

</header>

<div id="content" class="content sized-width-content">
