// =============================================
//  gulpfile.js
// =============================================

/**
 * @Project:      wpnewspaper
 * @Autor:        Gabriele Proietti Mattia <gabry3795>
 * @Email:        gabry.gabry@hotmail.it
 * @Created on:   Feb 09, 2017
 * @Modified by:  gabry3795
 * @Modified on:  Feb 10, 2017
 * @License:      GNU Public License 3.0
 */

const { parallel, series, src, dest } = require('gulp');

// const concat = require("gulp-concat");
// const uglify = require("gulp-uglify");
const changed = require("gulp-changed");
const imagemin = require("gulp-imagemin");
const del = require("del");
// const browser_sync = require("browser-sync").create();
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const googleWebFonts = require("gulp-google-webfonts");

let outputDir = "./public"

// new gulp file
function libs(cb) {
  return src("node_modules/@fortawesome/fontawesome-free/css/all.min.css")
    .pipe(rename("font-awesome.min.css"))
    .pipe(dest(`${outputDir}/libs/css`)) &&
    src("node_modules/@fortawesome/fontawesome-free/webfonts/**/*.*")
      .pipe(dest(`${outputDir}/libs/webfonts`));
}

function fonts(cb) {
  return src("src/fonts.list")
    .pipe( googleWebFonts({ fontsDir: "", cssDir: "" }))
    .pipe(dest(`${outputDir}/fonts`));
}

function sassT(cb) {
  return src("src/sass/style.scss")
    .pipe(sourcemaps.init())
    //.pipe(concat('style.scss'))
    .pipe(sass().on("error", sass.logError))
    .pipe(sourcemaps.write("."))
    .pipe(dest(outputDir))
}

function images(cb) {
  var imgSrc = "src/images/**/*.+(png|jpg|gif)",
    imgDst = `${outputDir}/img/`;

  return src(imgSrc)
    .pipe(changed(imgDst))
    .pipe(imagemin())
    .pipe(dest(imgDst));
}

function php(cb) {
  return src("src/**/*.php").pipe(dest(outputDir));
}

function clean(cb) {
  let compiled_folders = ["libs", "customizer", "menus", "widgets", "fonts"];
  let compiled_filetypes = ["*.css", "*.map", "*.php"];
  del.sync(compiled_folders);
  del.sync(compiled_filetypes);
  del.sync(outputDir)
  cb();
}

// run

let build = series(
  parallel(libs, sassT, images),
  parallel(php),
);
exports.clean = clean
exports.build = build
exports.default = series(clean, build);