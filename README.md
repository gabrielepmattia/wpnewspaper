<!--
@Author: Gabriele Proietti Mattia <gabry3795>
@Date:   Feb 09, 2017
@Email:  gabry.gabry@hotmail.it
@Project: wpnewspaper
@Filename: README.md
@Last modified by:   gabry3795
@Last modified time: Feb 09, 2017
@License: GNU Public License 3.0
@Copyright: See /LICENSE for full license text
-->

# WPNewsPaper
*Wordpress Theme*

[![build status](https://gitlab.com/gabry3795/wpnewspaper/badges/master/build.svg)](https://gitlab.com/gabry3795/wpnewspaper/commits/master)

# Description
This is a simple wordpress theme for a newspaper
